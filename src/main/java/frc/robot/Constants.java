// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.Unit;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean
 * constants. This class should not be used for any other purpose. All constants
 * should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
  public static final class ModuleConstants {
    public static final double kMaxSpeed = 3;
    public static final double kMaxAngularSpeed = Math.PI;
    public static final double kWheelRadius = Units.inchesToMeters(2);
    public static final double kWheelDiameter = kWheelRadius*2;
    public static final double kWheelCircumference = 2*Math.PI*kWheelRadius;
    public static final double kDrivingGearRatio = 9;

    private final static Translation2d m_frontLeftLocation = new Translation2d(0.3683, 0.3683);
    private final static Translation2d m_frontRightLocation = new Translation2d(0.3683, -0.3683);
    private final static Translation2d m_backLeftLocation = new Translation2d(-0.3683, 0.3683);
    private final static Translation2d m_backRightLocation = new Translation2d(-0.3683, -0.3683);

    public final static SwerveDriveKinematics m_kinematics =
      new SwerveDriveKinematics(
          m_frontLeftLocation, m_frontRightLocation, m_backLeftLocation, m_backRightLocation);

  }
  public static final class TimeConstants{
    public static final double SecondsInMinutes = 60;
  }
  public static final class HIDConstants {
    public static final double kJoystickDeadband = 0.09;
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;


import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.trajectory.TrapezoidProfile;

import edu.wpi.first.wpilibj.AnalogEncoder;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.math.util.Units;
import frc.robot.Constants.*;
import frc.robot.subsystems.Drivetrain;

public class SwerveModule {

  private static final double kModuleMaxAngularVelocity = Drivetrain.kMaxAngularSpeed;
  private static final double kModuleMaxAngularAcceleration =
      2 * Math.PI; // radians per second squared

  private final CANSparkMax m_driveMotor;
  private final CANSparkMax m_turningMotor;

  private final RelativeEncoder m_driveEncoder;
  private final AnalogInput m_turningEncoderInput;
  private int VoltageTypePublic = 0;

  // Gains are for example purposes only - must be determined for your own robot!
  private final PIDController m_drivePIDController = new PIDController(1, 0, 0);
 
  // Gains are for example purposes only - must be determined for your own robot!
  private final ProfiledPIDController m_turningPIDController =
      new ProfiledPIDController(
          1,
          0,
          0,
          new TrapezoidProfile.Constraints(
              kModuleMaxAngularVelocity, kModuleMaxAngularAcceleration));
  

  // Gains are for example purposes only - must be determined for your own robot!
  private final SimpleMotorFeedforward m_driveFeedforward = new SimpleMotorFeedforward(1, 3);
  private final SimpleMotorFeedforward m_turnFeedforward = new SimpleMotorFeedforward(1, 0.5);

  /**
   * Constructs a SwerveModule with a drive motor, turning motor, drive encoder and turning encoder.
   *
   * @param driveMotorChannel CAN ID of the driving Spark Max.
   * @param turningMotorChannel CAN ID of the turning Spark Max.
   * @param turningMotorEncoderID Analog ID of the Analog Encoder for the turning motor.
   */
  public SwerveModule(
      int driveMotorChannel,
      int turningMotorChannel,
      int turningMotorEncoderID,
      int VoltageType) {
    m_driveMotor = new CANSparkMax(driveMotorChannel, MotorType.kBrushless);
    m_turningMotor = new CANSparkMax(turningMotorChannel,MotorType.kBrushless);
    m_driveMotor.setIdleMode(IdleMode.kBrake);
    m_turningMotor.setIdleMode(IdleMode.kBrake);
    m_driveMotor.burnFlash();
    m_turningMotor.burnFlash();

    m_driveEncoder = m_driveMotor.getEncoder();
    //TODO
    m_turningEncoderInput = new AnalogInput(turningMotorEncoderID);
    VoltageTypePublic = VoltageType;
    
    // Set the distance per pulse for the drive encoder. We can simply use the
    // distance traveled for one rotation of the wheel divided by the encoder
    // resolution.

    // TODO - Use meaningful constants so its readable
    // .setVelocityConversionFactor reports distance per minute, so convert that to distance per second by dividing by 60
    m_driveEncoder.setPositionConversionFactor(ModuleConstants.kWheelCircumference / ModuleConstants.kDrivingGearRatio);
    m_driveEncoder.setVelocityConversionFactor((ModuleConstants.kWheelCircumference)/(TimeConstants.SecondsInMinutes*ModuleConstants.kDrivingGearRatio));

    // Set the distance (in this case, angle) in radians per pulse for the turning encoder.
    // This is the the angle through an entire rotation (2 * pi) divided by the
    // encoder resolution.

    // Limit the PID Controller's input range between -pi and pi and set the input
    // to be continuous.
    m_turningPIDController.enableContinuousInput(-Math.PI, Math.PI);
    m_turningPIDController.setTolerance(0.2*Math.PI);
  }
  public double getVoltage(){
    double LampV = m_turningEncoderInput.getVoltage();
    double currentVoltages[] = {LampV};
    final double[][] voltageArray =
      {
        {0.00,0.19,0.37,0.57,0.72,0.88,1.01,1.18,1.34,1.52,1.66,1.83,2.01,2.16,
        2.32,2.44,2.60,2.77,2.91,3.10,3.23,3.43,3.54,3.70,3.83,4.01,4.17,4.33,4.49,4.61,4.74,4.89,5.01},// FL  
        {0.00,0.15,0.26,0.37,0.46,0.57,0.65,0.75,0.85,0.98,1.09,1.20,1.31,1.43,
        1.52,1.62,1.74,1.84,1.95,2.05,2.13,2.23,2.31,2.40,2.51,2.62,2.72,2.83,2.93,3.03,3.13,3.23,3.33},// FR
        {0.00,0.20,0.39,0.57,0.69,0.84,1.00,1.19,1.32,1.50,1.63,1.79,1.94,2.11,
        2.24,2.48,2.61,2.80,2.93,3.11,3.20,3.36,3.46,3.68,3.79,4.02,4.15,4.31,4.46,4.61,4.74,4.91,5.01},// BL
        {0.00,0.14,0.11,0.32,0.42,0.53,0.61,0.72,0.83,0.97,1.06,1.18,1.26,1.37,
        1.44,1.56,1.63,1.78,1.89,2.01,2.09,2.18,2.27,2.38,2.51,2.63,2.72,2.86,2.92,3.01,3.08,3.20,3.33}// BR
      };
    //double filteredVoltageDegrees = linearInterpolation(LampV, voltageArray, VoltageTypePublic);
   // double AbsolutePositionRadians = filteredVoltageDegrees*((Math.PI/180));
    return LampV;
  }
  public double getRadians(int VoltageType){
    if (VoltageType == 1){
      return (m_turningEncoderInput.getVoltage()/5)*(2*Math.PI);
    } else return (m_turningEncoderInput.getVoltage()/3.3*(2*Math.PI));
  }
  /**
   * 
   * @return The current position of the turning encoder
   */
  public double getTurningEncoderValue(){
    return getRadians(VoltageTypePublic);
  }
  /**
   * Returns the current state of the module.
   *
   * @return The current state of the module.
   */
  public SwerveModuleState getState() {
    return new SwerveModuleState(
        m_driveEncoder.getVelocity(), new Rotation2d(getRadians(VoltageTypePublic)));
  }

  /**
   * Returns the current position of the module.
   *
   * @return The current position of the module.
   */
  public SwerveModulePosition getPosition() {
    return new SwerveModulePosition(
        m_driveEncoder.getPosition(), new Rotation2d(getRadians(VoltageTypePublic)));
  }

  /**
   * Sets the desired state for the module.
   *
   * @param desiredState Desired state with speed and angle.
   */

  public void setDesiredState(SwerveModuleState desiredState) {

    var encoderRotation = new Rotation2d(getRadians(VoltageTypePublic));

    // Optimize the reference state to avoid spinning further than 90 degrees
    SwerveModuleState state = SwerveModuleState.optimize(desiredState, encoderRotation);

    // Scale speed by cosine of angle error. This scales down movement perpendicular to the desired
    // direction of travel that can occur when modules change directions. This results in smoother
    // driving.
    state.speedMetersPerSecond *= state.angle.minus(encoderRotation).getCos();

    // Calculate the drive output from the drive PID controller.
    final double driveOutput =
        m_drivePIDController.calculate(m_driveEncoder.getVelocity(), state.speedMetersPerSecond);

    final double driveFeedforward = m_driveFeedforward.calculate(state.speedMetersPerSecond);

    // Calculate the turning motor output from the turning PID controller.
    final double turnOutput =
        m_turningPIDController.calculate(getRadians(VoltageTypePublic), state.angle.getRadians());

    final double turnFeedforward =
        m_turnFeedforward.calculate(m_turningPIDController.getSetpoint().velocity);
    m_driveMotor.setVoltage(driveOutput + driveFeedforward);
    if (m_turningPIDController.atSetpoint()) {
      m_turningMotor.setVoltage(0);
    } else {
      m_turningMotor.setVoltage(turnOutput + turnFeedforward);
      }
    }

}

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class OuttakeSystem extends SubsystemBase {
    DoubleSolenoid m_OuttakeSolenoid = new DoubleSolenoid(PneumaticsModuleType.REVPH, 15, 15);
    public void MoveOuttake(){
        m_OuttakeSolenoid.toggle();
        Timer.delay(3);
        m_OuttakeSolenoid.set(DoubleSolenoid.Value.kOff);
    }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import org.littletonrobotics.junction.Logger;
import org.littletonrobotics.junction.AutoLogOutput;

import com.ctre.phoenix6.hardware.Pigeon2;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.ReplanningConfig;

import frc.robot.Constants.ModuleConstants;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.kinematics.struct.SwerveModuleStateStruct;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.SwerveModule;
import frc.utils.SwerveUtils;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.util.WPIUtilJNI;

/** Represents a swerve drive style drivetrain. */
public class Drivetrain extends SubsystemBase {
  private double m_currentRotation = 0.0;
  private double m_currentTranslationDir = 0.0;
  private double m_currentTranslationMag = 0.0;

  private SlewRateLimiter m_magLimiter = new SlewRateLimiter(1.8);
  private SlewRateLimiter m_rotLimiter = new SlewRateLimiter(2.0);
  private double m_prevTime = WPIUtilJNI.now() * 1e-6;

  public static final double kMaxSpeed = 3.0; // 3 meters per second
  public static final double kMaxAngularSpeed = Math.PI; // 1/2 rotation per second


  private final SwerveModule m_frontLeft = new SwerveModule(11,2,0,1);
  private final SwerveModule m_frontRight = new SwerveModule(6,5,3,2);
  private final SwerveModule m_backLeft = new SwerveModule(13,10,2,1);
  private final SwerveModule m_backRight = new SwerveModule(8,7,1,2);

  private final Pigeon2 m_gyro = new Pigeon2(9);

  @Override
  public void periodic(){
    updateOdometry();
    Logger.recordOutput("RobotState/rotation", Rotation2d.fromDegrees(m_gyro.getYaw().getValueAsDouble()));
    SmartDashboard.putNumber("Encoder FL Value", m_frontLeft.getTurningEncoderValue());
    SmartDashboard.putNumber("Encoder FR Value", m_frontRight.getTurningEncoderValue());
    SmartDashboard.putNumber("Encoder BL Value", m_backLeft.getTurningEncoderValue());
    SmartDashboard.putNumber("Encoder BR Value", m_backRight.getTurningEncoderValue());
  }

  private final SwerveDriveOdometry m_odometry =
      new SwerveDriveOdometry(
          ModuleConstants.m_kinematics,
          m_gyro.getRotation2d(),
          new SwerveModulePosition[] {
            m_frontLeft.getPosition(),
            m_frontRight.getPosition(),
            m_backLeft.getPosition(),
            m_backRight.getPosition()
          });

  public Drivetrain() {
    try {
      AutoBuilder.configureHolonomic(
    this::getPose,
    this::resetOdometry,
    this::getRobotRelativeSpeeds,
    this::driveRobotRelative,
    new HolonomicPathFollowerConfig(
      new PIDConstants(1,0,0),
      new PIDConstants(1,0,0),
      4.8, // max speed in m/s
      Units.inchesToMeters(Math.sqrt(Math.pow(Units.metersToInches(0.3683), 2)+Math.pow(Units.metersToInches(0.3683),2))/2), // Radius in meters of 28.5 x 18.5 inch robot using a^2 +b^2 = c^2
      new ReplanningConfig()
    ),
    () -> {
                    // Boolean supplier that controls when the path will be mirrored for the red alliance
                    // This will flip the path being followed to the red side of the field.
                    // THE ORIGIN WILL REMAIN ON THE BLUE SIDE

                    var alliance = DriverStation.getAlliance();
                    if (alliance.isPresent()) {
                        return alliance.get() == DriverStation.Alliance.Red;
                    }
                    return false;
                },
                this // Reference to this subsystem to set requirements
        );
    } catch (Exception e) {
      System.out.println("oopsie");
    }
  }

  /**
   * Method to drive the robot using joystick info.
   *
   * @param xSpeed Speed of the robot in the x direction (forward).
   * @param ySpeed Speed of the robot in the y direction (sideways).
   * @param rot Angular rate of the robot.
   * @param fieldRelative Whether the provided x and y speeds are relative to the field.
   */
  public void drive(double xSpeed, double ySpeed, double rot, boolean fieldRelative, boolean rateLimit) {
    
    double xSpeedCommanded;
    double ySpeedCommanded;
    if (rateLimit) {
      // Convert XY to polar for rate limiting
      double inputTranslationDir = Math.atan2(ySpeed, xSpeed);
      double inputTranslationMag = Math.sqrt(Math.pow(xSpeed, 2) + Math.pow(ySpeed, 2));

      // Calculate the direction slew rate based on an estimate of the lateral acceleration
      double directionSlewRate;
      if (m_currentTranslationMag != 0.0) {
        directionSlewRate = Math.abs(1.2 / m_currentTranslationMag);
      } else {
        directionSlewRate = 500.0; //some high number that means the slew rate is effectively instantaneous
      }
      

      double currentTime = WPIUtilJNI.now() * 1e-6;
      double elapsedTime = currentTime - m_prevTime;
      double angleDif = SwerveUtils.AngleDifference(inputTranslationDir, m_currentTranslationDir);
      if (angleDif < 0.45*Math.PI) {
        m_currentTranslationDir = SwerveUtils.StepTowardsCircular(m_currentTranslationDir, inputTranslationDir, directionSlewRate * elapsedTime);
        m_currentTranslationMag = m_magLimiter.calculate(inputTranslationMag);
      }
      else if (angleDif > 0.85*Math.PI) {
        if (m_currentTranslationMag > 1e-4) { //some small number to avoid floating-point errors with equality checking
          // keep currentTranslationDir unchanged
          m_currentTranslationMag = m_magLimiter.calculate(0.0);
        }
        else {
          m_currentTranslationDir = SwerveUtils.WrapAngle(m_currentTranslationDir + Math.PI);
          m_currentTranslationMag = m_magLimiter.calculate(inputTranslationMag);
        }
      }
      else {
        m_currentTranslationDir = SwerveUtils.StepTowardsCircular(m_currentTranslationDir, inputTranslationDir, directionSlewRate * elapsedTime);
        m_currentTranslationMag = m_magLimiter.calculate(0.0);
      }
      m_prevTime = currentTime;
      
      xSpeedCommanded = m_currentTranslationMag * Math.cos(m_currentTranslationDir);
      ySpeedCommanded = m_currentTranslationMag * Math.sin(m_currentTranslationDir);
      m_currentRotation = m_rotLimiter.calculate(rot);
      SmartDashboard.putNumber("Input X Speeds", xSpeedCommanded);
      SmartDashboard.putNumber("Input Y Speed", ySpeedCommanded);


    } else {
      xSpeedCommanded = xSpeed;
      ySpeedCommanded = ySpeed;
      m_currentRotation = rot;
    }

    // Convert the commanded speeds into the correct units for the drivetrain
    double xSpeedDelivered = xSpeedCommanded * ModuleConstants.kMaxSpeed;
    double ySpeedDelivered = ySpeedCommanded * ModuleConstants.kMaxSpeed;
    double rotDelivered = m_currentRotation * ModuleConstants.kMaxAngularSpeed;
    SwerveModuleState[] swerveModuleStates = ModuleConstants.m_kinematics.toSwerveModuleStates(
        fieldRelative
            ? ChassisSpeeds.fromFieldRelativeSpeeds(xSpeedDelivered, ySpeedDelivered, rotDelivered, m_gyro.getRotation2d())
            : new ChassisSpeeds(xSpeedDelivered, ySpeedDelivered, rotDelivered));
    SwerveDriveKinematics.desaturateWheelSpeeds(
        swerveModuleStates, ModuleConstants.kMaxSpeed);
    Logger.recordOutput("SwerveStates/wanted", swerveModuleStates);
    Logger.recordOutput("RobotState/rotation", Rotation2d.fromDegrees(m_gyro.getYaw().getValueAsDouble()));
    m_frontLeft.setDesiredState(swerveModuleStates[0]);
    m_frontRight.setDesiredState(swerveModuleStates[1]);
    m_backLeft.setDesiredState(swerveModuleStates[2]);
    m_backRight.setDesiredState(swerveModuleStates[3]);
  }

  /** Gets (and logs) the states of the swerve drive */
  @AutoLogOutput(key = "SwerveStates/measured")
  private SwerveModuleState[] getModuleStates() {
    return new SwerveModuleState[]
    {
      m_frontLeft.getState(),
      m_frontRight.getState(),
      m_backLeft.getState(),
      m_backRight.getState()
    };
  }

  /** Updates the field relative position of the robot. */
  public void updateOdometry() {
    m_odometry.update(
        m_gyro.getRotation2d(),
        new SwerveModulePosition[] {
          m_frontLeft.getPosition(),
          m_frontRight.getPosition(),
          m_backLeft.getPosition(),
          m_backRight.getPosition()
        });
  }
  /** 
   * @return An array of wheel states that get converted into a chassis state
   */
  public ChassisSpeeds getRobotRelativeSpeeds(){
    return ModuleConstants.m_kinematics.toChassisSpeeds(m_frontLeft.getState(),
            m_frontRight.getState(),
            m_backLeft.getState(),
            m_backRight.getState());
  }

  public void driveRobotRelative(ChassisSpeeds speeds){
    this.drive(speeds.vxMetersPerSecond,speeds.vyMetersPerSecond,speeds.omegaRadiansPerSecond,false,false);
  }
  public void resetOdometry(Pose2d pose) {
    m_odometry.resetPosition(
        m_gyro.getRotation2d(),
        new SwerveModulePosition[] {
            m_frontLeft.getPosition(),
            m_frontRight.getPosition(),
            m_backLeft.getPosition(),
            m_backRight.getPosition()
        },
        pose);
  }
  public Pose2d getPose() {
    return m_odometry.getPoseMeters();
  }
}
